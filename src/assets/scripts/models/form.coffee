# {"tableId":16,"team":"A","captain":"B","phone":"234534645","gameNum":"181","gameType":"qnq"}:
class iQuizTables.Models.Form extends Backbone.Model

	defaults:
		table_id: null
		team: null
		captain: null
		phone: null
		gameNum: null
		gameType: null

	validation:
		team:
			required: true
		captain:
			required: true
		phone:
			required: true
			pattern: 'digits'
			minLength: 7
			maxLength: 10

	initialize: (attrs, options) ->
		@set
			gameNum: options.gameId
			gameType: options.gameName

	save: ->

		@trigger 'loading'
		@xhr = $.ajax
			method: 'post'
			url: 'http://cq.md/iquiz_tables/tables/backend/set_tables.php',
			data: JSON.stringify @toJSON()
			success: (response) =>
				if response is 'exists'
					alert('К сожалению, этот столик уже занят.')

				@clear()
				@trigger 'ready'
				iQuizTables.trigger 'table:saved'

	clear: ->
		@set
			table_id: null
			team: null
			captain: null
			phone: null
