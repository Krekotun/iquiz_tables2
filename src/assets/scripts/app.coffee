$ ->

	iQuizTables = window.iQuizTables ? {
		Views: {}
		Collections: {}
		Models: {}
		Templates: {}
	}

	window.iQuizTables.Templates =
		Table: Handlebars.compile $('#table-template').html()
		TableOrgs: Handlebars.compile $('#org-table-template').html()

	# events bus
	iQuizTables extends Backbone.Events

	class iQuizTables.App
		constructor: ->
			$el = $('.iquiz_tables')
			options = JSON.parse $('.iquiz_tables').attr('data-options')

			@collection = new iQuizTables.Collections.Tables 0, {
				model: iQuizTables.Models.Table
				gameName: options.gameName
				gameId: options.gameId
			}

			@view = new iQuizTables.Views.Base
				model: new iQuizTables.Models.Base()
				el: $el
				collection: @collection
				gameName: options.gameName
				gameId: options.gameId

		close: ->
			@view.close?()
			@view.remove?()

	new iQuizTables.App

	return iQuizTables
