class iQuizTables.Views.InfoPopup extends Backbone.View

	initialize: (options) ->

	open: ($el, model) ->
		@$el.show()

		height = @$el.outerHeight()
		width = @$el.outerWidth()
		elWidth = $el.width()

		@$el.css
			left: $el.position().left - (width/2) + (elWidth/2) + 'px'
			top: $el.position().top - height - 30 + 'px'

		@$el.find('.iquiz_tables--table_info').text( model.get('team') )

	close: ->
		@$el.hide()
