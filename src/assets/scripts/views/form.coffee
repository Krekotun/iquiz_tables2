class iQuizTables.Views.Form extends Backbone.View

	events:
		'submit': 'submit'

	initialize: (options)->

		@addListeners()
		@addStickit()
		@addValidation()
		@stickit()

	addListeners: ->
		@listenTo @model, 'loading', =>
			@$el.addClass('-loading')

		@listenTo @model, 'ready', =>
			@$el.removeClass('-loading')
			@close()

	addStickit: ->
		@bindings =
			'[name="team"]': 'team'
			'[name="captain"]': 'captain'
			'[name="phone"]': 'phone'

	addValidation: (options) ->
		Backbone.Validation.bind @,
			selector: options?.selector ? 'name'
			valid: options?.valid ? (view, attr, selector) ->
				if attr.indexOf('.') isnt -1
					attr = attr.split('.')
					$field = view.$el.find('[' + selector + '*="' + attr[0] + '[' + attr[1] + ']"]')
				else
					$field = view.$el.find('[' + selector + '*="' + attr + '"]')

				$field.removeClass('-error')
				$field.parents('.field, .select').find('.field--error').html('')

			invalid: options?.invalid ? (view, attr, error, selector) ->
				if attr.indexOf('.') isnt -1
					attr = attr.split('.')
					$field = view.$el.find('[' + selector + '*="' + attr[0] + '[' + attr[1] + ']"]')
				else
					$field = view.$el.find('[' + selector + '*="' + attr + '"]')

				$field.addClass('-error')

	submit: (e) ->
		e?.preventDefault?()
		unless @model.validate()
			@model.save()

	open: ($el, model) ->
		@$el.show()

		height = @$el.outerHeight()
		width = @$el.outerWidth()
		elWidth = $el.width()

		@$el.css
			left: $el.position().left - (width/2) + (elWidth/2) + 'px'
			top: $el.position().top - height - 30 + 'px'

		@model.set('table_id', model.get('table_id'))

	close: ->
		@model.clear()
		@$el.hide()
