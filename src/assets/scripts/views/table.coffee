window.iQuizTables = window.iQuizTables ? {
	Views: {}
	Collections: {}
	Models: {}
	Templates: {}
}

class iQuizTables.Views.Table extends Backbone.View

	className: 'iquiz_tables--table'

	events:
		'click': 'openPopup'

	initialize: (options) ->
		@addForm = options.addForm
		@infoPopup = options.infoPopup

		@addStickit()
		@addListeners()

	addStickit: ->
		@bindings =
			':el':
				classes:
					'-reserved': 'isReserved'

	addListeners: ->
		@listenTo @, 'rendered', =>
			@stickit()

	render: ->
		@$el.addClass('-n_' + @model.get('table_id') )
		@$el.html( iQuizTables.Templates.Table({
			table_id: @model.get('table_id')
			}) )

		@trigger 'rendered'

		return @

	openPopup: ->
		@addForm.close()
		@infoPopup.close()
		unless @model.get('isReserved')
			@addForm.open(@$el, @model)
		else
			@infoPopup.open(@$el, @model)
